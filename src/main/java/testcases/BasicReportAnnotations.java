package testcases;

import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class BasicReportAnnotations {
	
	ExtentHtmlReporter reporter;
	ExtentReports reports;
	
	@BeforeSuite
	public void startReport()
	{
		reporter = new ExtentHtmlReporter("./reports/result.html");
		reporter.setAppendExisting(true);
		
        reports = new ExtentReports();
		reports.attachReporter(reporter);
	}
	
	@Test
	public void report() throws IOException
	{

		
		ExtentTest test = reports.createTest("CreateLead","CreatingtheLead");
		test.pass("Username entered Successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/snap1.png").build());
		
		
				
	}
	
	@AfterSuite
	public void stopReport()
	{
		reports.flush();
	}
	}
