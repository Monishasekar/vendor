package testcases;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

@Test
public class UiPath {

	public void acme_vendor() throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		driver.get("https://acme-test.uipath.com/account/login");
		driver.manage().window().maximize();
		driver.findElementById("email").sendKeys("monishasekar95@gmail.com");
		driver.findElementById("password").sendKeys("Siddheshwaran123");
		driver.findElementById("buttonLogin").click();
		
		Thread.sleep(3000);
		
		Actions action = new Actions(driver);
		WebElement vendors = driver.findElementByXPath("(//button[@type = 'button'])[6]");
		action.moveToElement(vendors).perform();
		Thread.sleep(3000);
		
		driver.findElementByLinkText("Search for Vendor").click();
		driver.findElementById("vendorTaxID").sendKeys("FR121212");
		driver.findElementById("buttonSearch").click();
		Thread.sleep(3000);
		
		System.out.println(driver.findElementByXPath("(//th[text() = 'Vendor']/following::tr/td)[1]").getText());
		
		
		
		
	}
	
}
