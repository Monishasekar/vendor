package testcases;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;


public class BasicReport {

	@Test
	public void report() throws IOException
	{
		ExtentHtmlReporter reporter = new ExtentHtmlReporter("./reports/result.html");
		reporter.setAppendExisting(true);
		
		ExtentReports reports = new ExtentReports();
		reports.attachReporter(reporter);
		
		ExtentTest test = reports.createTest("CreateLead","CreatingtheLead");
		test.pass("Username entered Successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/snap1.png").build());
		reports.flush();
		
				
		
	}
}
