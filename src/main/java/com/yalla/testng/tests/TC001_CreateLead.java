package com.yalla.testng.tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TC001_CreateLead extends Annotations{
	//@BeforeTest(groups = "any")
	@BeforeTest
	public void setData() {
		testcaseName= "TC001_CreateLead";
		testcaseDec = "Create a new Lead in leaftaps";
		author      = "Gayatri";
		category    = "Smoke";
	}
	@DataProvider(name = "createData") 
		
	public Object[][] fetchData() {

		Object [][] data = new Object[2][3];
		data[0][0] = "wipro";
		data[0][1] = "Monisha";
		data[0][2] = "S";
		
		data[1][0] = "wipro";
		data[1][1] = "Jayanthi";
		data[1][2] = "V";
		
	    return data;
		
	}
	
	//@Test(groups = "smoke")
	@Test(dataProvider = "createData")
	public void createLead(String companyName, String fName, String lName) {
		click(locateElement("link", "CRM/SFA"));
		click(locateElement("link", "Leads"));
		click(locateElement("link", "Create Lead"));
		clearAndType(locateElement("id", "createLeadForm_companyName"),companyName);
		clearAndType(locateElement("id", "createLeadForm_firstName"),fName);
		clearAndType(locateElement("id", "createLeadForm_lastName"),lName);
		click(locateElement("name", "submitButton")); 
	}
	
}
