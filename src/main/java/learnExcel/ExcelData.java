package learnExcel;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelData {

	public static void main(String[] args) throws IOException {
		
		XSSFWorkbook wBook = new XSSFWorkbook("./data/exceldata.xlsx");
		XSSFSheet sheet = wBook.getSheetAt(0);
		wBook.close();
		
		int rowCount = sheet.getLastRowNum();
		System.out.println("The count of rows:" +rowCount);
		short colCount = sheet.getRow(0).getLastCellNum();
		System.out.println("The count of columns:" +colCount);
		
		for(int i = 1; i <= rowCount; i++) {
			XSSFRow row = sheet.getRow(i);
			
			for(int j = 0; j < colCount; j++) 
			{
				
				XSSFCell col = row.getCell(j);
				String stringCellValue = col.getStringCellValue();
				System.out.println(stringCellValue);
				
		}
			
}
	
//		XSSFRow row = sheet.getRow(1);
//		XSSFCell col = row.getCell(0);
//		String stringCellValue = col.getStringCellValue();
//		System.out.println(stringCellValue);
//		
		
		
		
		
		

	}

}
